﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Software_Licence_Management.Config
{
    public interface DatabaseCompatible
    {
        String Delete();

        String Insert();

        String Update();

        String selectAll();

        DatabaseCompatible ConvertFromMySqlDataReader(MySqlDataReader MyDataReader);

        String selectById();

        String getRawSelectSql();

    }
}
