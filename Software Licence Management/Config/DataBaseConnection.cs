﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Software_Licence_Management.Config
{
    public class DataBaseConnection
    {

        private static DataBaseConnection me;

        private DataBaseConnection()
        {
        }

        private MySqlConnection getNewConnection()
        {
            return new MySqlConnection("Persist Security Info=False;database=software_licence_management;server=localhost;Connect Timeout=30;uid=root; pwd=''; convert zero datetime=True");
        }

        public static DataBaseConnection Instance()
        {
            if (DataBaseConnection.me == null)
            {
                DataBaseConnection.me = new DataBaseConnection();
            }
            return DataBaseConnection.me;
        }

        public List<DatabaseCompatible> GetData(DatabaseCompatible wow)
        {
            MySqlConnection connection = this.getNewConnection();
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(wow.selectAll(), connection);
            MySqlDataReader MyDataReader;
            MyDataReader = cmd.ExecuteReader();
            List<DatabaseCompatible> dataToReturn = new List<DatabaseCompatible>();
            while (MyDataReader.Read())
            {
                //System.Diagnostics.Debug.WriteLine(MyDataReader.GetString("surname"));
                dataToReturn.Add(wow.ConvertFromMySqlDataReader(MyDataReader));
            }
            MyDataReader.Dispose();
            cmd.Dispose();
            connection.Close();
            connection.Dispose();
            return dataToReturn;
        }

        public List<DatabaseCompatible> GetDataByRawQry(DatabaseCompatible wow)
        {
            MySqlConnection connection = this.getNewConnection();
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(wow.getRawSelectSql(), connection);
            MySqlDataReader MyDataReader;
            MyDataReader = cmd.ExecuteReader();
            List<DatabaseCompatible> dataToReturn = new List<DatabaseCompatible>();
            while (MyDataReader.Read())
            {
                dataToReturn.Add(wow.ConvertFromMySqlDataReader(MyDataReader));
            }
            MyDataReader.Dispose();
            cmd.Dispose();
            connection.Close();
            connection.Dispose();
            return dataToReturn;
        }

        public void insetData(DatabaseCompatible wow)
        {
            MySqlConnection connection = this.getNewConnection();
            connection.Open();
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = wow.Insert();
            comm.ExecuteNonQuery();
            comm.Dispose();
            connection.Close();
            connection.Dispose();
        }

        public void updateData(DatabaseCompatible wow)
        {
            MySqlConnection connection = this.getNewConnection();
            connection.Open();
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = wow.Update();
            comm.ExecuteNonQuery();
            comm.Dispose();
            connection.Close();
            connection.Dispose();
        }

        public void deleteData(DatabaseCompatible wow)
        {
            MySqlConnection connection = this.getNewConnection();
            connection.Open();
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = wow.Delete();
            comm.ExecuteNonQuery();
            comm.Dispose();
            connection.Close();
            connection.Dispose();
        }

        public DatabaseCompatible GetDataById(DatabaseCompatible wow)
        {
            MySqlConnection connection = this.getNewConnection();
            connection.Open();
            MySqlCommand cmd = new MySqlCommand(wow.selectById(), connection);
            MySqlDataReader MyDataReader;
            MyDataReader = cmd.ExecuteReader();
            DatabaseCompatible dataToReturn = null;
            while (MyDataReader.Read())
            {
                dataToReturn = wow.ConvertFromMySqlDataReader(MyDataReader);
            }
            MyDataReader.Dispose();
            cmd.Dispose();
            connection.Close();
            connection.Dispose();
            return dataToReturn;
        }


    }
}
