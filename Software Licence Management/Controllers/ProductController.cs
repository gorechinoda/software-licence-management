﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Software_Licence_Management.Config;
using Software_Licence_Management.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Software_Licence_Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        // GET: api/<ProductController>
        [HttpGet]
        public List<Product> Get()
        {
            List<DatabaseCompatible> data = DataBaseConnection.Instance().GetData(new Product());
            List<Product> dataChairo = new List<Product>();
            data.ForEach(aa => {
                Product x = (Product)aa;
                dataChairo.Add(x);
            });
            return dataChairo;
        }

        // GET api/<ProductController>/5
        [HttpGet("{id}")]
        public Product Get(int id)
        {
            Product product = new Product();
            product.ID = id;
            product = (Product)DataBaseConnection.Instance().GetDataById(product);
            return product;
        }

        // POST api/<ProductController>
        [HttpPost]
        public Product Post(Product value)
        {
            DataBaseConnection.Instance().insetData(value);
            return value;
        }

        // PUT api/<ProductController>/5
        [HttpPut("{id}")]
        public String Put(int id, Product value)
        {
            Product product = new Product();
            product.ID = id;
            product = (Product)DataBaseConnection.Instance().GetDataById(product);
            if (product == null)
            {
                return "Product not found";
            }
            else if (product.ID == value.ID)
            {
                DataBaseConnection.Instance().updateData(value);
                return "updated";
            }
            else
            {
                return "Error trying to update wrong product with wrong id";
            }
        }

        // DELETE api/<ProductController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Product value = new Product();
            value.ID = id;
            DataBaseConnection.Instance().deleteData(value);
        }
    }
}
