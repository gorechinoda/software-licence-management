﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Software_Licence_Management.Config;
using Software_Licence_Management.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Software_Licence_Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        // GET: api/<UserController>
        [HttpGet]
        public List<User> Get()
        {
            List<DatabaseCompatible> data = DataBaseConnection.Instance().GetData(new User());
            List<User> dataChairo = new List<User>();
            data.ForEach(aa => {
                User x = (User)aa;
                dataChairo.Add(x);
            });
            return dataChairo;
        }

        // GET api/<UserController>/5
        [HttpGet("{id}")]
        public User Get(int id)
        {
            User user = new User();
            user.ID = id;
            user = (User)DataBaseConnection.Instance().GetDataById(user);
            return user;
        }

        // POST api/<UserController>
        [HttpPost]
        public User Post(User value)
        {
            
            DataBaseConnection.Instance().insetData(value);
            return value;
        }

        // PUT api/<UserController>/5
        [HttpPut("{id}")]
        public String Put(int id, User value)
        {
            User user = new User();
            user.ID = id;
            user = (User)DataBaseConnection.Instance().GetDataById(user);
            if (user == null)
            {
                return "User not found";
            }
            else if (user.ID == value.ID)
            {
                DataBaseConnection.Instance().updateData(value);
                return "updated";
            }
            else
            {
                return "Error trying to update wrong user with wrong id";
            }
        }

        // DELETE api/<UserController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            User value = new User();
            value.ID = id;
            DataBaseConnection.Instance().deleteData(value);
        }

        // Login api/<UserController>/login
        [HttpPost("login")]
        public User Login(User value)
        {
            value.RawSql = value.login();
            List<DatabaseCompatible> reso = DataBaseConnection.Instance().GetDataByRawQry(value);
            if (reso.Count > 0)
            {
                return (User)reso.ElementAt(0);
            }
            return null;
        }
    }
}
