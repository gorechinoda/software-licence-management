﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Software_Licence_Management.Config;
using Software_Licence_Management.Model;
using Software_Licence_Management.Security;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Software_Licence_Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LicenceController : ControllerBase
    {
        // GET: api/<LicenceController>
        [HttpGet]
        public List<Licence> Get()
        {
            List<DatabaseCompatible> data = DataBaseConnection.Instance().GetData(new Licence());
            List<Licence> dataChairo = new List<Licence>();
            data.ForEach(aa => {
                Licence x = (Licence)aa;
                dataChairo.Add(x);
            });
            return dataChairo;
        }

        // GET api/<LicenceController>/5
        [HttpGet("{id}")]
        public Licence Get(int id)
        {
            Licence licence = new Licence();
            licence.ID = id;
            licence = (Licence)DataBaseConnection.Instance().GetDataById(licence);
            return licence;
        }

        // POST api/<LicenceController>
        [HttpPost]
        public Licence Post(Licence value)
        {
            DataBaseConnection.Instance().insetData(value);
            return value;
        }

        // PUT api/<LicenceController>/5
        [HttpPut("{id}")]
        public string Put(int id, Licence value)
        {
            Licence licence = new Licence();
            licence.ID = id;
            licence = (Licence)DataBaseConnection.Instance().GetDataById(licence);
            if (licence == null)
            {
                return "Licence not found";
            }
            else if (licence.ID == value.ID)
            {
                DataBaseConnection.Instance().updateData(value);
                return "updated";
            }
            else
            {
                return "Error trying to update wrong licence with wrong id";
            }
        }

        // DELETE api/<LicenceController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Licence value = new Licence();
            value.ID = id;
            DataBaseConnection.Instance().deleteData(value);
        }

        // Login api/<UserController>/login
        [HttpPost("login")]
        public Licence Login(Licence value)
        {
            value.RawSql = value.login();
            List<DatabaseCompatible> reso = DataBaseConnection.Instance().GetDataByRawQry(value);
            if (reso.Count > 0)
            {
                return (Licence)reso.ElementAt(0);
            }
            return null;
        }
    }
}
