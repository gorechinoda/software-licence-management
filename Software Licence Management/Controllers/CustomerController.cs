﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Software_Licence_Management.Config;
using Software_Licence_Management.Model;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Software_Licence_Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        // GET: api/<CustomerController>
        [HttpGet]
        public List<Customer> Get()
        {
            List<DatabaseCompatible> data = DataBaseConnection.Instance().GetData(new Customer());
            List<Customer> dataChairo = new List<Customer>();
            data.ForEach(aa => {
                Customer x = (Customer)aa;
                dataChairo.Add(x);
            });
            return dataChairo;
        }

        // GET api/<CustomerController>/5
        [HttpGet("{id}")]
        public Customer Get(int id)
        {
            Customer customer = new Customer();
            customer.ID = id;
            customer = (Customer)DataBaseConnection.Instance().GetDataById(customer);
            return customer;
        }

        // POST api/<CustomerController>
        [HttpPost]
        public Customer Post(Customer value)
        {
            DataBaseConnection.Instance().insetData(value);
            return value;
        }

        // PUT api/<CustomerController>/5
        [HttpPut("{id}")]
        public String Put(int id, Customer value)
        {
            Customer customer = new Customer();
            customer.ID = id;
            customer = (Customer)DataBaseConnection.Instance().GetDataById(customer);
            if(customer == null)
            {
                return "Customer not found";
            }else if(customer.ID == value.ID)
            {
                DataBaseConnection.Instance().updateData(value);
                return "updated";
            }
            else
            {
                return "Error trying to update wrong customer with wrong id";
            }
        }

        // DELETE api/<CustomerController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Customer value = new Customer();
            value.ID = id;
            DataBaseConnection.Instance().deleteData(value);
        }
    }
}
