﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Software_Licence_Management.Config;
using MySql.Data.MySqlClient;
using System.Data;
using Software_Licence_Management.Security;

namespace Software_Licence_Management.Model
{
    public class Licence : DatabaseCompatible
    {
        public int ID { get; set; }

        public int CustomerId { get; set; }

        public int ProductId { get; set; }

        public String LicenceKey { get; set; }

        [DataType(DataType.Date)]
        public DateTime ExpiryDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

        public int Deleted { get; set; }

        public int Valid { get; set; }

        public String RawSql { get; set; }

        public Product product { get; set; }

        public bool status { get; set; }

        String DatabaseCompatible.getRawSelectSql()
        {
            return this.RawSql;
        }

        DatabaseCompatible DatabaseCompatible.ConvertFromMySqlDataReader(MySqlDataReader MyDataReader)
        {
            Licence licence = new Licence();
            licence.ID = MyDataReader.GetInt32("id");
            licence.CustomerId = MyDataReader.GetInt32("customer_id");
            licence.ProductId = MyDataReader.GetInt32("product_id");
            licence.LicenceKey = MyDataReader.GetString("licence_key");
            licence.ExpiryDate = MyDataReader.GetDateTime("expiry_date");
            licence.DateCreated = MyDataReader.GetDateTime("date_created");
            licence.Deleted = MyDataReader.GetInt32("deleted");
            licence.Valid = MyDataReader.GetInt32("valid");
            licence.status = licence.ExpiryDate.CompareTo(DateTime.Now)>0;
            Product product = new Product();
            product.RawSql = "Select * from product where deleted = 0 and id = " + licence.ProductId;
            List<DatabaseCompatible> wow = DataBaseConnection.Instance().GetDataByRawQry(product);
            if (wow.Count > 0)
            {
                licence.product = (Product)wow.ElementAt(0);
            }            
            return licence;
        }

        string DatabaseCompatible.Delete()
        {
            return "update licence set deleted = 1 WHERE id = " + this.ID;
        }

        string DatabaseCompatible.Insert()
        {
            this.LicenceKey = Encryption.MD5Hash(this.ExpiryDate.ToString() + ":" + this.ProductId.ToString() + "-" + this.CustomerId.ToString());
            String date = this.ExpiryDate.Year + "-" + this.ExpiryDate.Month + "-" + this.ExpiryDate.Day;
            return "INSERT INTO licence(customer_id,product_id,licence_key,expiry_date,valid,deleted) VALUES ('"+this.CustomerId+"','"+this.ProductId+"','"+this.LicenceKey+"','"+date+"','1','0')";
        }

        String DatabaseCompatible.selectAll()
        {
            return "Select * from licence where deleted = 0";
        }

        string DatabaseCompatible.Update()
        {
            this.LicenceKey = Encryption.MD5Hash(this.ExpiryDate.ToString() + ":" + this.ProductId.ToString() + "-" + this.CustomerId.ToString());
            return "update licence set customer_id = '" + this.CustomerId + "',product_id='" + this.ProductId + "',licence_key='" + this.LicenceKey + "',expiry_date='" + this.ExpiryDate + "',valid='" + this.Valid + "',deleted = '" + this.Deleted + "' where id = "+this.ID;
        }

        string DatabaseCompatible.selectById()
        {
            return "select * from licence where id = " + this.ID;
        }

        public string login()
        {
            //this.LicenceKey = Encryption.MD5Hash(this.LicenceKey);
            return "select * from licence where licence_key = '" + this.LicenceKey + "'";
        }
    }
}
