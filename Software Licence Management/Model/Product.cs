﻿using MySql.Data.MySqlClient;
using Software_Licence_Management.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Software_Licence_Management.Model
{
    public class Product : DatabaseCompatible
    {
        public int ID { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

        public String Name { get; set; }
        public int Deleted { get; set; }

        public String RawSql { get; set; }

        String DatabaseCompatible.getRawSelectSql()
        {
            return this.RawSql;
        }

        DatabaseCompatible DatabaseCompatible.ConvertFromMySqlDataReader(MySqlDataReader MyDataReader)
        {
            Product licence = new Product();
            licence.ID = MyDataReader.GetInt32("id");
            licence.Name = MyDataReader.GetString("name");
            licence.DateCreated = MyDataReader.GetDateTime("date_created");
            licence.Deleted = MyDataReader.GetInt32("deleted");
            return licence;
        }

        string DatabaseCompatible.Delete()
        {
            return "update product set deleted = 1 WHERE id = " + this.ID;
        }

        string DatabaseCompatible.Insert()
        {
            return "INSERT INTO product(name,deleted) VALUES ('" + this.Name + "','0')";
        }

        string DatabaseCompatible.selectAll()
        {
            return "select * from product where deleted = 0";
        }

        string DatabaseCompatible.selectById()
        {
            return "select * from product where id = " + this.ID;
        }

        string DatabaseCompatible.Update()
        {
            return "update product set name = '" + this.Name + "',deleted = '" + this.Deleted + "' where id = "+this.ID;
        }
    }
}
