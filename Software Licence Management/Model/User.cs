﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Software_Licence_Management.Config;
using MySql.Data.MySqlClient;
using Software_Licence_Management.Security;

namespace Software_Licence_Management.Model
{
    public class User : DatabaseCompatible
    {
        public int ID { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

        public int Deleted { get; set; }

        public String RawSql { get; set; }

        String DatabaseCompatible.getRawSelectSql()
        {
            return this.RawSql;
        }

        DatabaseCompatible DatabaseCompatible.ConvertFromMySqlDataReader(MySqlDataReader MyDataReader)
        {
            User user = new User();
            user.ID = MyDataReader.GetInt32("id");
            user.Username = MyDataReader.GetString("username");
            user.Password = MyDataReader.GetString("password");
            user.DateCreated = MyDataReader.GetDateTime("date_created");
            user.Deleted = MyDataReader.GetInt32("deleted");
            return user;
        }

        string DatabaseCompatible.Delete()
        {
            return "delete from users WHERE id = " + this.ID;
        }

        string DatabaseCompatible.Insert()
        {
            this.Password = Encryption.MD5Hash(this.Password);
            return "INSERT INTO users(username,password,deleted) VALUES ('" + this.Username + "','" + this.Password + "','0')";
        }

        string DatabaseCompatible.selectAll()
        {
            return "Select * FROM users where deleted = 0";
        }

        string DatabaseCompatible.Update()
        {
            this.Password = Encryption.MD5Hash(this.Password);
            return "update users set username = '" + this.Username + "',password = '" + this.Password + "'  where id = "+this.ID;
        }

        string DatabaseCompatible.selectById()
        {
            return "select * from users where id = " + this.ID;
        }

        public string login()
        {
            this.Password = Encryption.MD5Hash(this.Password);
            return "select * from users where username = '"+this.Username+"' and password = '"+this.Password+"'";
        }
    }
}
