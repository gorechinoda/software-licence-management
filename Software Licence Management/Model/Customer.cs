﻿using MySql.Data.MySqlClient;
using Software_Licence_Management.Config;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Software_Licence_Management.Model
{
    public class Customer : DatabaseCompatible
    {
        public Customer() { }
        public int ID { get; set; }
        public string Title { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateCreated { get; set; }

        public int Deleted { get; set; }

        public String RawSql { get; set; }

        public int Licences { get; set; }

        public List<Licence> LicencesList { get; set; }

        DatabaseCompatible DatabaseCompatible.ConvertFromMySqlDataReader(MySqlDataReader MyDataReader)
        {
            System.Diagnostics.Debug.WriteLine(MyDataReader.GetString("surname"));
            Customer wow = new Customer();
            wow.Surname = MyDataReader.GetString("surname");
            wow.Name = MyDataReader.GetString("firstname");
            wow.Title = MyDataReader.GetString("title");
            wow.ID = MyDataReader.GetInt32("id");
            wow.DateCreated = MyDataReader.GetDateTime("date_created");
            wow.Email = MyDataReader.GetString("email");
            Licence ls = new Licence();
            ls.RawSql = "Select * from licence where deleted = 0 and customer_id = " + wow.ID;
            List<DatabaseCompatible> licences = DataBaseConnection.Instance().GetDataByRawQry(ls);
            wow.Licences = licences.Count;
            wow.LicencesList = new List<Licence>();
            licences.ForEach(licence =>
            {
                wow.LicencesList.Add((Licence)licence);
            });
            return wow;
        }

        string DatabaseCompatible.Delete()
        {
            return "update customer set deleted = 1 WHERE id = " + this.ID;
        }

        string DatabaseCompatible.Insert()
        {
            return "INSERT INTO customer(firstname,surname,title,email,deleted) VALUES ('" + this.Name + "','" + this.Surname + "','" + this.Title + "','" + this.Email + "','0')";
        }

        string DatabaseCompatible.selectAll()
        {
            return "Select * FROM customer where deleted = 0";
        }

        string DatabaseCompatible.Update()
        {
            return "update customer set firstname='" + this.Name + "',surname = '" + this.Surname + "',title = '" + this.Title + "',email = '" + this.Email + "',deleted = '" + this.Deleted + "' where id = "+this.ID;
        }

        string DatabaseCompatible.selectById()
        {
            return "select * from customer where id = " + this.ID;
        }

        String DatabaseCompatible.getRawSelectSql()
        {
            return this.RawSql;
        }
    }
}
