import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MyHttpGetwayService } from './my-http-getway.service';

@Injectable({
  providedIn: 'root'
})
export class UserAuthGuard implements CanActivate {

  constructor(public http: MyHttpGetwayService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(!this.http.getLoginStaticState().customer){
        return true;
      }
      this.router.navigateByUrl("/pages/create-edit-customers/"+this.http.getLoginStaticState().id);
      return false;
  }

}
