import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { MyHttpGetwayService } from '../../../my-http-getway.service';
import { SmartTableData } from '../../../@core/data/smart-table';
import { Router } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
  selector: 'ngx-view-all-users',
  templateUrl: './view-all-users.component.html',
  styleUrls: ['./view-all-users.component.scss']
})
export class ViewAllUsersComponent implements OnInit {

  settings = {
    hideSubHeader:false,
    filter:false,
    actions:{
      position: 'right',
      add:false,
      delete:true,
      edit:true
    },
    mode: 'external',//|'inline',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      username: {
        title: 'Username',
        type: 'string',
      },
      dateCreated: {
        title: 'Date Created',
        type: 'string',
        valuePrepareFunction: (value, row, cell) => {
          return formatDate(value,'dd-MM-yyyy','en_ZW');
        }
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  myUrl = "user";

  constructor(private service: SmartTableData,private http : MyHttpGetwayService
    ,private navigate:Router) {
    this.lodData();
  }

  ngOnInit(): void {
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.http.myDdelete(this.myUrl+"/"+event.data.id).subscribe(response=>{
        // event.confirm.resolve();
        this.lodData();
      })
    } else {

    }
  }

  onEdit(event):void{
    console.log(event.data);
    this.navigate.navigateByUrl("/pages/create-edit-user/"+event.data.id);
  }

  lodData(){
    this.http.myGet(this.myUrl).subscribe(data=>{
      this.source.load(data);
    });
  }

}
