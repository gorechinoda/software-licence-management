import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MyHttpGetwayService } from 'app/my-http-getway.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-create-edit-user',
  templateUrl: './create-edit-user.component.html',
  styleUrls: ['./create-edit-user.component.scss']
})
export class CreateEditUserComponent implements OnInit {

  userForm = new FormGroup({
    id: new FormControl(''),
    username: new FormControl('',[Validators.required]),
    password: new FormControl('',[Validators.required])
  });
  processing = false;
  id = 0;

  public constructor(private http : MyHttpGetwayService,
    private route:ActivatedRoute,
    private navigate: Router) {
      this.id = this.route.snapshot.params.id;
      if(this.id>0){
        this.http.myGet("user/"+this.id).subscribe(response=>{
          if(response){
            this.userForm.get('username').setValue(response.username);
            this.userForm.get('password').setValue(response.password);
          }
          else{
            this.id = 0;
            this.http.showToast("Error","Error getting the product");
          }
        });
      }
  }

  ngOnInit() {}

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.processing = true;
    this.userForm.get('id').setValue(Number(this.id));
    if(this.id>0){
      this.http.myPut("user/"+this.id,this.userForm.value).subscribe((response)=>{
        this.http.showToast("Succes","User updated successfully")
        this.processing = false;
        this.navigate.navigateByUrl('/pages/all-users');
      },error=>{
        this.http.showToast("Succes","User updated successfully")
        this.processing = false;
        this.navigate.navigateByUrl('/pages/all-users');
      });
    }else{
      this.http.myPost("user",this.userForm.value).subscribe((response)=>{
        this.http.showToast("Succes","User saved successfully")
        this.processing = false;
        this.navigate.navigateByUrl('/pages/all-users');
      });
    }
  }
}
