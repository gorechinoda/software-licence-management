import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MyHttpGetwayService } from 'app/my-http-getway.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  userLoginForm = new FormGroup({
    username: new FormControl('',[Validators.required]),
    password: new FormControl('',[Validators.required])
  });

  licenceForm = new FormGroup({
    key: new FormControl('',[Validators.required])
  });

  processing = false;
  id = 0;

  revealed = false;

  public constructor(private http : MyHttpGetwayService,
    private route:ActivatedRoute,
    private navigate: Router) {
  }

  ngOnInit() {}

  onSubmit() {
    this.processing = true;
    if(!this.revealed){
      this.http.myPost("user/login",this.userLoginForm.value).subscribe((response)=>{
        this.processing = false;
        if(response == null){
          return this.http.showToast("Error","User not found","danger");
        }
        localStorage.setItem("user",JSON.stringify(response));
        localStorage.setItem("userTpe","0");
        this.http.login();
        this.navigate.navigateByUrl('/pages/dashboard');
      });

    }else{
      this.http.myPost("licence/login",this.licenceForm.value).subscribe((response)=>{
        this.processing = false;
        if(response == null){
          return this.http.showToast("Error","Key not found","danger");
        }
        localStorage.setItem("user",JSON.stringify(response));
        localStorage.setItem("userType","1");
        this.http.login();
        this.navigate.navigateByUrl('/pages/create-edit-customers/'+response.customerId);
      });
      // localStorage.setItem("user",JSON.stringify({id:4}));
      // localStorage.setItem("userType","1");
      // this.navigate.navigateByUrl('/pages/create-edit-customers/'+4);
    }
  }

  toggleView() {
    this.revealed = !this.revealed;
  }

}
