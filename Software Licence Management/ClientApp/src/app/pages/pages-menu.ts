import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'grid-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Users',
    icon: 'edit-2-outline',
    children: [
      {
        title: 'All users',
        link: '/pages/all-users',
      },
      {
        title: 'Add new',
        link: '/pages/create-edit-user/0',
      },
    ],
  },
  {
    title: 'Customers',
    icon: 'browser-outline',
    children: [
      {
        title: 'All customers',
        link: '/pages/all-customers',
      },
      {
        title: 'Add new',
        link: '/pages/create-edit-customers/0',
      },
    ],
  },
  {
    title: 'Products',
    icon: 'shopping-cart-outline',
    children: [
      {
        title: 'All products',
        link: '/pages/all-products',
      },
      {
        title: 'Add new',
        link: '/pages/create-edit-products/0',
      },
    ],
  },

];
