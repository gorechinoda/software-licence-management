import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MyHttpGetwayService } from 'app/my-http-getway.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'ngx-create-edit-product',
  templateUrl: './create-edit-product.component.html',
  styleUrls: ['./create-edit-product.component.scss']
})
export class CreateEditProductComponent implements OnInit {

  productForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl('',[Validators.required])
  });
  processing = false;
  id = 0;

  constructor(private http : MyHttpGetwayService,
    private route:ActivatedRoute,
    private navigate: Router) {
      this.id = this.route.snapshot.params.id;
      this.productForm.get('id').setValue(Number(this.id));
      if(this.id>0){
        this.http.myGet("product/"+this.id).subscribe(response=>{
          if(response){
            this.productForm.get('name').setValue(response.name);
          }
          else{
            this.id = 0;
            this.http.showToast("Error","Error getting the product");
          }
        });
      }
     }

  ngOnInit(): void {
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.processing = true;
    if(this.id>0){
      this.http.myPut("product/"+this.id,this.productForm.value).subscribe((response)=>{
        this.http.showToast("Succes","Product updated successfully")
        this.processing = false;
        this.navigate.navigateByUrl('/pages/all-products');
      },error=>{
        this.http.showToast("Succes","Product updated successfully")
        this.processing = false;
        this.navigate.navigateByUrl('/pages/all-products');
      });
    }else{
      this.http.myPost("product",this.productForm.value).subscribe((response)=>{
        this.http.showToast("Succes","Product saved successfully")
        this.processing = false;
        this.navigate.navigateByUrl('/pages/all-products');
      });
    }
  }


}
