import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { ViewAllUsersComponent } from './user/view-all-users/view-all-users.component';
import { ViewAllComponent } from './customer/view-all/view-all.component';
import { CreateEditUserComponent } from './user/create-edit-user/create-edit-user.component';
import { CreateEditComponent } from './customer/create-edit/create-edit.component';
import { CreateEditProductComponent } from './product/create-edit-product/create-edit-product.component';
import { ViewAllProductsComponent } from './product/view-all-products/view-all-products.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { LoginAuthGuard } from '../login-auth.guard';
import { UserAuthGuard } from '../user-auth.guard';
import { CustomerAuthGuard } from '../customer-auth.guard';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'login',
      component: LoginPageComponent
    },
    {
      path: 'dashboard',
      component: ECommerceComponent,
      canActivate: [LoginAuthGuard,UserAuthGuard]
    },
    {
      path: 'all-users',
      component: ViewAllUsersComponent,
      canActivate: [LoginAuthGuard,UserAuthGuard]
    },
    {
      path: 'create-edit-user/:id',
      component: CreateEditUserComponent,
      canActivate: [LoginAuthGuard,UserAuthGuard]
    },
    {
      path: 'all-customers',
      component: ViewAllComponent,
      canActivate: [LoginAuthGuard,UserAuthGuard]
    },
    {
      path: 'create-edit-customers/:id',
      component: CreateEditComponent,
      canActivate: [LoginAuthGuard,CustomerAuthGuard]
    },
    {
      path: 'create-edit-products/:id',
      component: CreateEditProductComponent,
      canActivate: [LoginAuthGuard,UserAuthGuard]
    },
    {
      path: 'all-products',
      component: ViewAllProductsComponent,
      canActivate: [LoginAuthGuard,UserAuthGuard]
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
