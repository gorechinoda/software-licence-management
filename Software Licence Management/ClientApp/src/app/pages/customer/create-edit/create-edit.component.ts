import { Component, OnInit } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SmartTableData } from '../../../@core/data/smart-table';
import { MyHttpGetwayService, LoginState } from 'app/my-http-getway.service';
import { ActivatedRoute, Router } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
  selector: 'ngx-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss']
})
export class CreateEditComponent implements OnInit {

  customerProfileForm = new FormGroup({
    id: new FormControl(''),
    name: new FormControl('',[Validators.required]),
    surname: new FormControl('',[Validators.required]),
    title: new FormControl('',[Validators.required]),
    email: new FormControl('',[Validators.required,Validators.email]),
  });

  productKeyForm = new FormGroup({
    customerId: new FormControl(''),
    productId: new FormControl('',[Validators.required]),
    expiryDate: new FormControl('',[Validators.required])
  });

  processing = false;
  settings = {
    hideSubHeader:false,
    filter:false,
    actions:{
      position: 'right',
      add:false,
      delete:true,
      edit:false
    },
    mode: 'external',//|'inline',
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="rounded bg-danger text-white nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      product: {
        title: 'Product',
        type: 'string',
        valuePrepareFunction: (value, row, cell) => {
          return value.name;
        }
      },
      licenceKey: {
        title: 'Licence key',
        type: 'string',
      },
      status: {
        title: 'Licence State',
        type: 'html',
        valuePrepareFunction: (value, row, cell) => {
          if(value){
            return '<span class="badge badge-pill badge-success">Active</span>';
          }else{
            return '<span class="badge badge-pill badge-danger">Expired</span>';
          }
        }
      },
      expiryDate: {
        title: 'Expiry date',
        type: 'string',
        valuePrepareFunction: (value, row, cell) => {
          return formatDate(value,'dd-MM-yyyy','en_ZW');
        }
      },
      dateCreated: {
        title: 'Date created',
        type: 'string',
        valuePrepareFunction: (value, row, cell) => {
          return formatDate(value,'dd-MM-yyyy','en_ZW');
        }
      },

    },
  };

  source: LocalDataSource = new LocalDataSource();

  id = 0;
  isCustomer:boolean = false;
  customer;
  products;
  myUrl = "customer";

  constructor(private service: SmartTableData,
    private http : MyHttpGetwayService,
    private route:ActivatedRoute,private navigate:Router) {
      this.http.getLoginState().subscribe((loginState:LoginState)=>{
        this.isCustomer = loginState.customer;
        if(this.isCustomer){
          this.settings.actions.delete = false;
        }
      });
      this.loadCustomerData();
  }

  ngOnInit(): void {
  }

  loadCustomerData(){
    var id = this.route.snapshot.params.id;
    this.http.myGet(this.myUrl+"/"+id).subscribe(response=>{
      this.id = id;
      this.customer = response;
      if(this.customer == null){
        this.id = 0;
        return;
      }
      this.id = this.customer.id;
      this.customerProfileForm.get('id').setValue(this.id);
      this.customerProfileForm.get('name').setValue(this.customer.name);
      this.customerProfileForm.get('surname').setValue(this.customer.surname);
      this.customerProfileForm.get('title').setValue(this.customer.title);
      this.customerProfileForm.get('email').setValue(this.customer.email);
      this.source.load(this.customer.licencesList);
      this.http.myGet("product").subscribe(response=>{
        this.products = response;
      });
    });
  }

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      this.http.myDdelete("licence/"+event.data.id).subscribe(respons=>{
        this.loadCustomerData();
      });
    } else {
      event.confirm.reject();
    }
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.processing = true;
    if(this.id>0){
      this.http.myPut(this.myUrl+"/"+this.id,this.customerProfileForm.value).subscribe((response)=>{
        this.http.showToast("Succes","Customer updated successfully")
        this.processing = false;
        this.loadCustomerData();
      },completed=>{
        this.http.showToast("Succes","Customer updated successfully")
        this.processing = false;
        this.loadCustomerData();
      });
    }else{
      this.http.myPost(this.myUrl,this.customerProfileForm.value).subscribe((response)=>{
        this.http.showToast("Succes","Customer saved successfully")
        this.processing = false;
        this.navigate.navigateByUrl('/pages/all-customers')
      });
    }
  }

  onSubmitProductKey(){
    this.productKeyForm.get("customerId").setValue(this.id);
    var data = this.productKeyForm.value;
    data.expiryDate = formatDate(data.expiryDate,'yyyy-MM-dd','en_ZW');
    data.productId =  Number(data.productId);
    // console.log(data);
    // return;
    this.http.myPost("licence",data).subscribe((response)=>{
      this.http.showToast("Succes","Licence created successfully");
      this.loadCustomerData();
      this.productKeyForm.get("customerId").setValue(this.id);
      this.productKeyForm.get("productId").setValue('');
      this.productKeyForm.get("expiryDate").setValue('');
      this.processing = false;
    });

  }

}
