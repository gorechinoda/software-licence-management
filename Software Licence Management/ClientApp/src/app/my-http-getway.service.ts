import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import {
  NbComponentStatus,
  NbGlobalPhysicalPosition,
  NbToastrService,
} from '@nebular/theme';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MyHttpGetwayService {

  private loginState: BehaviorSubject<LoginState>;
  types: NbComponentStatus[] = [
    'primary',
    'success',
    'info',
    'warning',
    'danger',
  ];

  baseUrl = "https://localhost:44395/api/";
  status: NbComponentStatus = 'success';


  constructor(private http: HttpClient,private toastrService: NbToastrService,
    private route:Router) {
    this.loginState = new BehaviorSubject({
      logged: false,
      customer: null,
      id:0
    });
    this.login();
  }

  public myGet(url): Observable<any>{
    return this.http.get(this.baseUrl+url);
  }

  public myPost(url,data): Observable<any>{
    return this.http.post(this.baseUrl+url,data);
  }

  public myDdelete(url): Observable<any>{
    return this.http.delete(this.baseUrl+url)
  }

  public myPut(url,data): Observable<any>{
    return this.http.put(this.baseUrl+url,data);
  }

  public showToast( title: string, body: string,status?) {
    let alt : NbComponentStatus = status;
    const config = {
      status: status == null ? this.status : alt,
      destroyByClick: true,
      duration: 8000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = 'Title';

    this.toastrService.show(
      body,
      title,
      config
      );
  }

  public login(){
    var user:any = localStorage.getItem("user");
    if(user){
      user = JSON.parse(user);
      var userType = Number(localStorage.getItem("userType"));
      this.loginState.next(
        {
          logged:true,
          customer: userType == 1,
          id: userType == 1 ? user.customerId : user.id
        }
      );
    }else{
      this.loginState.next(
        {
          logged:false,
          customer: null,
          id: 0
        }
      );
    }
  }

  public logout(){
    localStorage.clear();
    this.login();
    this.route.navigateByUrl("/pages/login");
  }

  public getLoginState():BehaviorSubject<LoginState>{
    return this.loginState;
  }

  public getLoginStaticState():LoginState{
    return this.loginState.value;
  }

}


export interface LoginState{
  logged:boolean;
  customer:boolean;
  id:number;
}
