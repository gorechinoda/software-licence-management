import { TestBed } from '@angular/core/testing';

import { MyHttpGetwayService } from './my-http-getway.service';

describe('MyHttpGetwayService', () => {
  let service: MyHttpGetwayService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyHttpGetwayService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
