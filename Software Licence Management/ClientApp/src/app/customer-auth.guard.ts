import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { MyHttpGetwayService } from './my-http-getway.service';
import { url } from 'inspector';

@Injectable({
  providedIn: 'root'
})
export class CustomerAuthGuard implements CanActivate {

  constructor( private http: MyHttpGetwayService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      var wow = state.url.split("/");
      var navigationId = Number(wow[wow.length-1]);
      if(!this.http.getLoginStaticState().customer){
        return true;
      }
      if(this.http.getLoginStaticState().id == navigationId){
        return true;
      }

      this.router.navigateByUrl("/pages/create-edit-customers/"+this.http.getLoginStaticState().id);
      return false;
  }

}
