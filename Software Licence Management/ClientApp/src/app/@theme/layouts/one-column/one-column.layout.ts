import { Component } from '@angular/core';
import { MyHttpGetwayService, LoginState } from 'app/my-http-getway.service';

@Component({
  selector: 'ngx-one-column-layout',
  styleUrls: ['./one-column.layout.scss'],
  template: `
    <nb-layout windowMode>
      <nb-layout-header fixed *ngIf="loggedIn">
        <ngx-header></ngx-header>
      </nb-layout-header>

      <nb-sidebar class="menu-sidebar" tag="menu-sidebar" responsive *ngIf="loggedIn && (null==customer || !customer)">
        <ng-content select="nb-menu"></ng-content>
      </nb-sidebar>

      <nb-layout-column>
        <ng-content select="router-outlet"></ng-content>
      </nb-layout-column>

      <nb-layout-footer fixed>
        <ngx-footer></ngx-footer>
      </nb-layout-footer>
    </nb-layout>
  `,
})
export class OneColumnLayoutComponent {
  loggedIn:boolean = false;
  customer: boolean = true;
  public constructor(private http : MyHttpGetwayService) {
    this.http.getLoginState().subscribe((loginState:LoginState)=>{
      this.loggedIn = loginState.logged;
      this.customer = loginState.customer;
    });
  }


}
